<?php
session_start();
/** Initialisation de l'autoloading et du router ******************************/

require('src/Autoloader.php');
Autoloader::register();

$router = new router\Router(basename(__DIR__));

/** Définition des routes *****************************************************/

// GET "/"
$router->get('/', 'controller\IndexController@index');

$router->get("/store","controller\StoreController@store");

$router->get("/store/{:num}", "controller\StoreController@product");

$router->get("/account","controller\AccountController@account");

$router->post("/account/login","controller\AccountController@login");

$router->post("/account/signin","controller\AccountController@signin");

$router->get("/account/logout","controller\AccountController@logout");

$router->post("/store/{:num}/postComment","controller\CommentController@postComment");

$router->post("/search","controller\StoreController@search");

$router->get("/account/infos","controller\AccountController@infos");

$router->post("/account/update","controller\AccountController@update");

$router->get('/cart',"controller\CartController@cart");

$router->post("/cart/add","controller\CartController@addProduct");

$router->post("/cart/update","controller\CartController@update");

$router->post("/store/{:num}/deleteComment","controller\CommentController@deleteComment");

$router->get("/account/wallet","controller\AccountController@wallet");

$router->post("/account/wallet/update","controller\AccountController@walletUpdate");

$router->post("/cart/order","controller\CartController@order");

// Erreur 404
$router->whenNotFound('controller\ErrorController@error');

/** Ecoute des requêtes entrantes *********************************************/

$router->listen();
