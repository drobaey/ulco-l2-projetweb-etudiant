document.addEventListener("DOMContentLoaded", function () {

    let form=document.querySelector("form")
    let btn=form.querySelector("input[type='submit']")
    let lastname=form.querySelector("input[name='lastname']")
    let firstname=form.querySelector("input[name='firstname']")
    let mail=form.querySelector("input[name='mail']")

    btn.addEventListener('click',function (event) {
        event.preventDefault()

        let valid_form = true

        //Les nom et prénom doivent contenir au minimum 2 caractères.

        if (lastname.value.length >= 2) {
            lastname.className = 'valid'
            lastname.previousElementSibling.className = 'valid'
        } else {
            valid_form = false
            lastname.className = 'invalid'
            lastname.previousElementSibling.className = 'invalid'
        }

        if (firstname.value.length >= 2) {
            firstname.className = 'valid'
            firstname.previousElementSibling.className = 'valid'
        } else {
            valid_form = false
            firstname.className = 'invalid'
            firstname.previousElementSibling.className = 'invalid'
        }

        //Le champ mail doit respecter le format d'une adresse mail.
        if (/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(mail.value)) {
            mail.className = 'valid'
            mail.previousElementSibling.className = 'valid'
        } else {
            valid_form = false
            mail.className = 'invalid'
            mail.previousElementSibling.className = 'invalid'
        }

        //submit du formulaire si il est valide
        if (valid_form) {
            form.submit()
        }
    })
})