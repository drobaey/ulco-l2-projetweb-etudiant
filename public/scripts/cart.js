document.addEventListener("DOMContentLoaded",function (){

    let quantite_produits = document.querySelectorAll(".cart-product")
    let total_price=document.querySelector("#cart-bottom #total-price")
    let total_price_int=parseInt(total_price.innerHTML)
    console.log(total_price_int)

    for (let product of quantite_produits) {
        let buttons= product.querySelectorAll("button") //buttons[0] = bouton - ; product[1] = quantité affiché ; product[2] = bouton + ;
        let input_hidden=product.querySelector("input")
        let nb=buttons[1].innerHTML;
        let product_price=parseInt(product.querySelector(".product-price-div .product-price").innerHTML)

        buttons[0].addEventListener("click",function (){
            if (nb!==0) {
                nb--
                buttons[1].innerHTML=nb
                input_hidden.value=nb
                total_price_int-=product_price
                total_price.innerHTML=total_price_int
            }
        })

        buttons[2].addEventListener("click",function (){
            if (nb<5) {
                nb++
                buttons[1].innerHTML=nb
                input_hidden.value=nb
                total_price_int+=product_price
                total_price.innerHTML=total_price_int
            }
        })

    }
})