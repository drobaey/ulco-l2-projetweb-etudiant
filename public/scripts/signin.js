document.addEventListener("DOMContentLoaded",function (){

    let form=document.querySelector("form.account-signin")
    let btn=form.querySelector("input[type='submit']")
    let lastname=form.querySelector("input[name='userlastname']")
    let firstname=form.querySelector("input[name='userfirstname']")
    let mail=form.querySelector("input[name='usermail']")
    let password=form.querySelectorAll("input[name='userpass']")


    btn.addEventListener('click',function (event) {
        event.preventDefault()

        let valid_form=true

        //Les nom et prénom doivent contenir au minimum 2 caractères.

        if (lastname.value.length>=2){
            lastname.className='valid'
            lastname.previousElementSibling.className='valid'
        } else {
            valid_form=false
            lastname.className='invalid'
            lastname.previousElementSibling.className='invalid'
        }

        if (firstname.value.length>=2) {
            firstname.className='valid'
            firstname.previousElementSibling.className='valid'
        } else {
            valid_form=false
            firstname.className='invalid'
            firstname.previousElementSibling.className='invalid'
        }


        //Le champ mail doit respecter le format d'une adresse mail.
        if (/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(mail.value)) {
            mail.className='valid'
            mail.previousElementSibling.className='valid'
        } else {
            valid_form=false
            mail.className='invalid'
            mail.previousElementSibling.className='invalid'
        }

        //Le mot de passe doit contenir au minimum 6 caractères, dont au moins 1 chiffre et 1 lettre.
        //Les deux champs mot de passe doivent être identiques.

        let pwd=password[0].value

        if (pwd.length>=6 && pwd===password[1].value && pwd.search(/\d/)!==-1 && pwd.search(/[a-zA-Z]/)!==-1) {
            for (let p of password) {
                p.className='valid'
                p.previousElementSibling.className='valid'
            }
        } else {
            valid_form=false
            for (let p of password) {
                p.className='invalid'
                p.previousElementSibling.className='invalid'
            }
        }

        //submit du formulaire si il est valide
        if (valid_form) {
            form.submit()
        }

    })
})