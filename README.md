# L2 Informatique - Projet Web

## Description

Développement d'une application simplifiée de e-commerce.

## Auteur

ROBAEY Dennis

## Liens utiles

- [Sujet](https://florian-lepretre.herokuapp.com/teaching/projetweb/sujet/)
- [Dépôts des étudiants](https://docs.google.com/spreadsheets/d/16ydvylkxeVydqQASeoj1vYBUgb6zJ1EWZZEMHhHzSQ8/edit?usp=sharing)

## Ne pas oublier de ne pas télécharger la nouvelle base de donnée