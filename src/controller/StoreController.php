<?php

namespace controller;

use model\StoreModel;
use function Sodium\add;

class StoreController {

  public function store(): void
  {
    // Communications avec la base de données
    $categories = \model\StoreModel::listCategories();
    $products = \model\StoreModel::listProduct();

    // Variables à transmettre à la vue
    $params = array(
      "title" => "Store",
      "module" => "store.php",
      "categories" => $categories,
      "products" => $products
    );

    // Faire le rendu de la vue "src/view/Template.php"
    \view\Template::render($params);
  }

  public function product(int $id): void
  {
      $infoProducts = \model\StoreModel::infoProduct($id);
      $comments = \model\CommentModel::listComment($id);

      if (count($infoProducts)==0) {
          header("Location: /store");
          exit();
      } else {
          $params = array(
              "title" => "Store",
              "module" => "product.php",
              "infoProduct" => $infoProducts,
              "comment" => $comments
          );

          \view\Template::render($params);
      }
  }

  public function search(): void
  {
      $categories = \model\StoreModel::listCategories();

      $research = "";
      if (!empty($_POST['search'])){
          $research=$_POST['search'];
      }

      $category ="";
      if (!empty( $_POST['category'])){
          $category=$_POST['category'];
      }

      $order = "";
      if (!empty($_POST['order'])) {
          $order=$_POST['order'];
      }

      $products = StoreModel::listSpecificProduct($research, $category, $order);

      $params = array(
          "title" => "Store",
          "module" => "store.php",
          "categories" => $categories,
          "products" => $products
      );

      \view\Template::render($params);
  }

}