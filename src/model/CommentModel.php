<?php


namespace model;


class CommentModel
{
    static function insertComment($content, $idProduct, $idAccount): void
    {
        $db = \model\Model::connect(); //ajout du commentaire dans la BDD
        $sql = "INSERT INTO comment(content, date, id_product, id_account) VALUES (?, now(), ?, ?)";

        // Exécution de la requête
        $req = $db->prepare($sql);
        $req->execute(array($content,$idProduct,$idAccount));
    }

    static function listComment($id_product): array
    {
        $db = \model\Model::connect();
        $sql = "SELECT account.firstname, account.lastname, comment.content, comment.id_account, comment.id  FROM comment INNER JOIN account on account.id=comment.id_account  WHERE comment.id_product=?";

        // Exécution de la requête
        $req = $db->prepare($sql);
        $req->execute(array($id_product));
        return $req->fetchAll();
    }

    static function deleteComment($id_comment): void
    {
        $db = \model\Model::connect();
        $sql = "DELETE FROM comment WHERE comment.id=?";

        // Exécution de la requête
        $req = $db->prepare($sql);
        $req->execute(array($id_comment));
    }

}

?>