<?php
    if (!empty($_GET)) {
        if ($_GET["status"]=="signin_success") {
            echo "<div class='box info' style='margin-left: 50px'>Inscription réussi ! Vous pouvez dès à présent vous connecter.</div>";
        }
        if ($_GET["status"]=="signin_fail") {
            echo "<div class='box error' style='margin-left: 50px'>L'inscription a échoué. L'adresse mail a déjà était utilisée.</div>";
        }
        if ($_GET["status"]=="login_fail") {
            echo "<div class='box error' style='margin-left: 50px'>La connexion a échoué. Vérifiez vos identifiants et réessayez.</div>";
        }
        if ($_GET["status"]=="logout") {
            echo "<div class='box info' style='margin-left: 50px'>Vous vous êtes déconnecté. À bientôt !</div>";
        }
        if ($_GET["status"]=="disconnected") {
            echo "<div class='box info' style='margin-left: 50px'>Veuillez-vous connecter.</div>";
        }
    }
?>


<div id="account">

<form class="account-login" method="post" action="/account/login">

  <h2>Connexion</h2>
  <h3>Tu as déjà un compte ?</h3>

  <p>Adresse mail</p>
  <input type="text" name="usermail" placeholder="Adresse mail" />

  <p>Mot de passe</p>
  <input type="password" name="userpass" placeholder="Mot de passe" />

  <input type="submit" value="Connexion" />

</form>

<form class="account-signin" method="post" action="/account/signin">

  <h2>Inscription</h2>
  <h3>Crée ton compte rapidement en remplissant le formulaire ci-dessous.</h3>

  <p>Nom</p>
  <input type="text" name="userlastname" placeholder="Nom" />

  <p>Prénom</p>
  <input type="text" name="userfirstname" placeholder="Prénom" />

  <p>Adresse mail</p>
  <input type="text" name="usermail" placeholder="Adresse mail" />

  <p>Mot de passe</p>
  <input type="password" name="userpass" placeholder="Mot de passe" />

  <p>Répéter le mot de passe</p>
  <input type="password" name="userpass" placeholder="Mot de passe" />

  <input type="submit" value="Inscription" />

</form>

</div>
<script src="/public/scripts/signin.js"></script>
