<?php if (!empty($_SESSION['user'])) {
    if (!empty($_GET)) {
        if ($_GET['status']=='update_success') {
            echo "<div class='box info' style='margin-left: 50px'>Tes informations personnelles ont été mises à jour!</div>";
        }

        if ($_GET['status']=='email_already_used') {
            echo "<div class='box error' style='margin-left: 50px'>La modification a échouée. L'adresse email est déjà utilisée.</div>";
        }

        if ($_GET['status']=='order_success') {
            echo "<div class='box info' style='margin-left: 50px'>Votre achat a bien été éffectué !</div>";
        }

    } ?>

    <div id="infos">
        <h2 class="h2-infos"> Informations du compte </h2 >
        <h3 class="h3-infos"> Informations personnelles </h3>
        <form id="form-infos" method="post" action="/account/update">
            <div><div>Prénom</div><input type="text" name="firstname" value = "<?= $_SESSION['user']['firstname']?>" /></div>
            <div><div>Nom</div><input type="text" name="lastname" value = "<?= $_SESSION['user']['lastname']?>" /> </div>
            <div><div>Adresse mail</div></label><input type="text" name="mail" value = "<?= $_SESSION['user']['mail']?>" /> </div>
            <input type="submit" value="Modifier mes informations"/>
        </form>

        <h3 class="h3-infos">Commandes</h3>
        <?php if (empty($params['ordered_products'])) { ?>
        <p>Tu n'as pas de commande en cours.</p>
        <?php } else {
                echo "<div id='cart'>"; //ce n'est pas le panier mais j'utilise le même css..
                foreach ($params['ordered_products'] as $ordered_product){ ?>

                    <div class="cart-product">
                        <p class="card-image"><img src="/public/images/<?= $ordered_product['image'] ?>"/> </p>

                        <div style="width: 400px" >
                            <p class="card-category"><?=$ordered_product['category_name']?></p>
                            <p class="card-title"><a href="/store/<?= $ordered_product['id_product']?>"><?= $ordered_product['product_name']?></a></p>
                        </div>
                        <div class="cart-quantity">
                            <p class="quantity">Quantité : </p>
                            <p class="price"><?= $ordered_product['quantity']?></p>
                        </div>
                        <div class="product-price-div" style="margin-left: 30px">
                            <p class="card-price">Prix unitaire : </p>
                            <p class="price"><span class="product-price">
                                    <?php if ($ordered_product['promotion']) {
                                        echo "<strike>$ordered_product[price]</strike> " . $ordered_product['price']/2 ;
                                    } else {
                                        echo $ordered_product['price'];
                                    } ?> </span>€
                            </p>
                        </div>
                        <div class="product-price-div" style="margin-left: 80px">
                            <p class="card-price">Livraison : </p>
                            <p>Un jour, peut être</p>
                        </div>

                    </div>

                <?php }
                echo "<div>";
            }
        ?>

    </div>
    <script src="/public/scripts/infos.js"></script>

<?php } else {
    header("Location: /account?status=disconnected");
}?>